<?php
//$Id$

/**
 * @file
 * Implementation of admin panel for duplicate_validator module
 */



/**
 * This form implements admin panel for duplicate validator module.
 *
 * @return array
 */
function duplicate_validator_configuration_form() {
  return system_settings_form(array(
    'duplicate_validator_check_comments' => array(
      '#type' => 'checkbox',
      '#title' => t('Enable checking of duplicates for comments.'),
      '#default_value' => variable_get('duplicate_validator_check_comments', FALSE)
    ),
    'duplicate_validator_check_node_types' => array(
      '#title' => t('Enable checking of duplicates for nodes'),
      '#type' => 'checkboxes',
      '#description' => t('Select nodes, which should be validated for duplication possibility'),
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('duplicate_validator_check_node_types', array())
    )
  ));
}